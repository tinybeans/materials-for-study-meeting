var api = new MT.DataAPI({
  baseUrl:  "https://seminar1211a-4yf7.trials.jp/mt/mt-data-api.cgi",
  clientId: "bitpart"
});


var createCount = 10;
var authoredOn = new Date();
var authoredOnTime = authoredOn.getTime();
api.authenticate({
    mtDataApiLoginMagicToken: jQuery("input[name='magic_token']").val()
  },
  function(response) {
    if (response.error) {
      alert("Error:" + response.error.message);
      return;
    }
    var successCount = 0;
    var entryData = {
      date: "2013-01-11T12:48:12+09:00",
      keywords: "Simple,Smart,Speedy",
      excerpt: "MT::Love::bitpart",
      body: [
        "<h2>About Movable Type</h2>",
        "<p>Movable Type has several notable features, such as the ability to host multiple weblogs and standalone content pages, manage files, user roles, templates, tags, categories, and trackback links.</p>",
        "<p>The application supports static page generation (in which files for each page are updated whenever the content of the site is changed), dynamic page generation (in which pages are composited from the underlying data as the browser requests them), or a combination of the two techniques. Movable Type optionally supports LDAP for user and group management and automatic blog provisioning.</p>",
        "<p>Movable Type is written in Perl, and supports storage of the weblog's content and associated data within MySQL natively. PostgreSQL and SQLite support was available prior to version 5, and can still be used via plug-ins. Movable Type Enterprise also supports the Oracle database and Microsoft SQL Server.</p>",
        "<p>Movable Type offers free software under the GPLv2 license. In addition to the free version, users can purchase support or buy commercial, education, or nonprofit licenses, which come with support contracts, author limits, and unlimited blogs.</p>"
      ].join("\n"),
      tags: ["sample"]
    };
    for (var i = 1; i <= createCount; i++) {
      // Set Entry Date
      entryData.title = "Hello Movable Type vol." + i;
      entryData.date = authoredOn.toISOString();
      api.createEntry(mtappVars.blog_id, entryData, function(response) {
        if (response.error) {
          alert("Error:" + response.error.message);
          return;
        }
        console.log(response);
        successCount++;
        console.log("Created: " + successCount);
        if (createCount === successCount) {
          alert("Created " + successCount + " entries.");
          location.reload();
        }
      });
      // Date
      authoredOnTime = authoredOnTime - (1000 * 24 * 60 * 60);
      authoredOn.setTime(authoredOnTime);
    }
  }
);