# Materials for study meeting

## 準備

Bitwordのテーマをインストール（ウェブサイトのテーマに変更）

## Data APIの基本

* スライドでData APIの概要を説明
	* 公式のドキュメントを見てみる  
https://github.com/movabletype/Documentation/wiki/Movable-type-data-api
* 受け取るJSONの表示を変えるChrome拡張紹介  
https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc
	* Firefox
	https://addons.mozilla.org/ja/firefox/addon/jsonview/
	* Safari
	https://github.com/rfletcher/safari-json-formatter

* Makanaiでmynaviのときのように説明
* Makanaiのendpoint叩いてみる（上の説明とそろえる）
	* makanaiの実例を見せてURLを叩いて比較
	* 叩くパラメータを用意する（entries、categories）
* Data API JavaScript SDK で、認証、記事の作成、取得、更新、削除のデモ（インデックステンプレートを作る）
* Bitwordで動いているのを見てみる
	* BitwordでData APIを使っている部分を見る
* 
* できたらPHPでエントリーを取得してみる

## サンプルサイトの話
・DataAPIを使ったサイトを見てみる
### DataAPI部分の解説
・トップ追加読み込み方法を知る
・検索＆タグアーカイブの実装方法を知る
・追加読み込み部分のjsのコード部分についての詳細解説
・プラグインによるData APIの拡張方法

## デモサイトを使ったその他のData APIの使い方
・認証
・投稿／編集／ステータス変更
・再構築など

---

・fancylistingとDataAPI
・・エントリーグループ
・bitcatchの検索をflexibleSearchとDataAPI
・デモサイトでつかうのはbitwordかbitcatch
・可能だったらbitwordにコメントを追加

・管理画面
・・材料
・・カテゴリラベル
・・・その場でラジオボタンにかえる